#/usr/bin/python3
import urllib.parse

class HttpHeader:
    url = None
    method = None
    version = None
    range_start = -1
    range_end = -1
    def __init__(self):
        pass

    def from_request(self, data):
        headers = data.decode().split("\r\n")
        #print(headers)
        for line in headers:
            if "GET" == line[:3]:
                self.method, self.url, self.protocol_version = line.split(" ")
                self.url = urllib.parse.unquote(self.url)
                print(self.method, self.url, self.protocol_version)
            if "Range: bytes=" in line:
                self.range_start = int(line[line.find("=")+1:line.find("-")])
                print("file range:",self.range_start)


#GET / HTTP/1.1\r\n
#Host: 192.168.137.20:8080\r\n
#Connection: keep-alive\r\n
#Cache-Control: max-age=0\r\n
#Upgrade-Insecure-Requests: 1\r\n
#User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36 Edg/113.0.1774.42\r\n
#Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7\r\n
#Accept-Encoding: gzip, deflate\r\n
#Accept-Language: zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6\r\n\r\n
#Range: bytes=185925632-\r\n
