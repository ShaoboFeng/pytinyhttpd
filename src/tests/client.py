import socket
import time
import os

class P2pClient:
    def __init__(self):
        self.tunnalServerHost = ''
        self.tunnalServerPort = 50007

    def run(self):
        p2pServerAddr = ConnTunnalServer()
        ConnPairServer(p2pServerAddr)

    def ConnTunnalServer(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.tunnalServerHost, self.tunnalServerPort))
        data = s.recv(1024)
        print(data)
        Addr = data.split("#")[1]
        return Addr
        s.close()

    def ConnPairServer(self, p2pServerAddr):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        addr = p2pServerAddr.split(":")
        s.connect((addr[0], int(addr[1])))
        while True:
            s.send("P2PServer")
            data = s.recv(1024)
            print(data)
            break
        s.close()

if __name__=="__main__":
    client = P2pClient()
    client.run()



