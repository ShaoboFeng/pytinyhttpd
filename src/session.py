#/usr/bin/env python3
import os
from src.http_protocol import HttpProtocol
from src.list_dir import ListDir 
from src.http_header import HttpHeader

class Session:
    def __init__(self, client_conn, conf):
        self.conn = client_conn
        self.server_dirs = conf.dirs
        self.conf = conf.conf
        self.hp = HttpProtocol(conf)
        self.listdir = ListDir(self.server_dirs)

    def process(self):
        try:
            data = self.conn.recv(1024)
            if data == None or len(data) == 0:
                return
            if len(data) == 1024:
                return
        except Exception as e:
            #print(e)
            return
        print(str(data))
        request_head = HttpHeader()
        request_head.from_request(data)
        #request_head = self.hp.get_req_header(data)
        
        has_index = False
        if request_head.url == "/":
            index_path = None
            for d in self.server_dirs:
                index_path = os.path.join(d, "index.html")
                if self.listdir.file_exists(index_path):
                    has_index = True
                    break
            if has_index:
                # response index.html
                http_response = self.hp.get_static_content(index_path, (request_head.range_start, request_head.range_end))
            elif self.conf.get("show_dirs"):
                # list config dir
                http_response = self.listdir.get_page()
            else:
                http_response = self.hp.get_error_page()
        else:
            request_head.url = request_head.url[1:]
            has_find = False
            has_dir = False
            cur_d = None
            req_url = None
            for d in self.server_dirs:
                first_dir_index = request_head.url.find("/")
                first_dir = request_head.url
                if first_dir_index != -1:
                    first_dir = request_head.url[:first_dir_index]
                #print(d[d.rfind("/")+1:], first_dir)
                if d[d.rfind("/")+1:] != first_dir:
                    req_url = os.path.join(d, request_head.url)
                else:
                    if first_dir_index == -1:
                        req_url = d
                    else:
                        req_url = os.path.join(d, request_head.url[request_head.url.find("/")+1:])
                cur_d = d
                if self.listdir.file_exists(req_url):
                    has_find = True
                    break
                if self.listdir.dir_exists(req_url):
                    has_dir = True
                    break
            #print(has_find, has_dir, req_url, cur_d)
            if has_find:
                http_response = self.hp.get_static_content(req_url, (request_head.range_start, request_head.range_end))
            elif self.conf.get("show_dirs") and has_dir:
                http_response = self.listdir.get_page(req_url, cur_d)
            else:
                # not find resource
                http_response = self.hp.get_error_page()
        self.conn.send(http_response)




