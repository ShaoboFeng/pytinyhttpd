# pytinyhttpd

#### 介绍
用python实现一个轻量级http server

项目背景：在内网搭建了一个自制NAS，需要使用手机在外网随时访问，django太过庞大，lighttpd是一个很好的选择，但是个人更喜欢python，对性能也没有太高的要求，所以决定自己写一个简单的web server。

#### 特性列表
|序号|特性名称|内容|进展|
|:----:|:----|:----|:----:|
|1|TCP会话通道建立|完成Socket监听，不同client完成|done|
|2|支持Http 200 状态|浏览器访问时，正确返回index.html的内容|100%|
|3|支持目录遍历|支持使用者配置目录，支持目录遍历功能|100%|
|4|支持图片格式|支持ico/png/jpg图片|100%|
|5|支持视频播放能力|支持播放视频|50%,支持mp4,断点续传|
|6|支持音频播放能力|支持mp3文件|100%|
|7|支持文档下载能力|支持文档下载|0%|
|8|支持sesssion能力|支持用户登录验证|0%|
|9|支持http 302状态|支持响应Http 302|0%|
#### 软件架构
软件架构说明


#### 安装教程

1.  pytinyhttpd 依赖python3

#### 使用说明

1.  python3 runserver [port]
2.  本项目支持内网搭建

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  作者个人网址：www.sbfeng.cn
